interface Aircraft {
    fun fly() :String
}
abstract class AircraftFactory {
    open fun create(type: String): Aircraft? = when (type) {
        "Boeing737" -> Boeing737()
        "Boeing777" -> Boeing777()
        else -> null
    }
}

open class Boeing737 : Aircraft {
    companion object Factory : AircraftFactory() {
        override fun create(type: String) = Boeing737()
        }
        override fun fly(): String {
        return ("Fly Boeing737!")
    }
}

open class Boeing777 : Aircraft {
    companion object Factory : AircraftFactory() {
        override fun create(type: String) = Boeing777()
        }
        override fun fly(): String {
        return ("Fly Boeing777!")
    }
}