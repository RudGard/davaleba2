interface Car {
    fun drive():String
}
abstract class CarFactory {
    open fun create(type: String): Car? = when (type) {
        "Audi" -> Audi()
        "Maserati" -> Maserati()
        else -> null
    }
}

open class Audi : Car {
    companion object Factory : CarFactory() {
        override fun create(type: String) = Audi()
        }
        override fun drive(): String {
        return ("Drive Audi!")
    }
}

open class Maserati : Car {
    companion object Factory : CarFactory(){
        override fun create(type: String) = Maserati()
        }
        override fun drive(): String {
        return ("Drive Maserati!")
    }
}

